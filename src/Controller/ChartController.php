<?php

namespace KarlitoWeb\Layouts\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Attribute\Route;

/**
 * Class ChartController
 *
 * @package KarlitoWeb\Layouts\Controller
 */
#[Route(path: '/layouts/{theme}/chart', name: 'kw.layouts.chart.', methods: ['GET'], format: 'html', utf8: true)]
final class ChartController extends AbstractController
{
	#[Route(path: '/chartjs.php', name: 'chartjs')]
    public function chartjs(string $theme): Response
    {
        return $this->render('@Layouts/themes/'.$theme.'/contents/chart/chartjs.html.twig', [
            'controller_name' => 'Welcome to Layouts',
        ]);
    }

	#[Route(path: '/apex.php', name: 'apex')]
    public function apex(string $theme): Response
    {
        return $this->render('@Layouts/themes/'.$theme.'/contents/chart/apexcharts.html.twig', [
            'controller_name' => 'Welcome to Layouts',
        ]);
    }

	#[Route(path: '/echart.php', name: 'echart')]
    public function echart(string $theme): Response
    {
        return $this->render('@Layouts/themes/'.$theme.'/contents/chart/echarts.html.twig', [
            'controller_name' => 'Welcome to Layouts',
        ]);
    }
}
