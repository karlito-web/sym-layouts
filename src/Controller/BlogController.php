<?php

namespace KarlitoWeb\Layouts\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Attribute\Route;

/**
 * Class BlogController
 *
 * @package KarlitoWeb\Layouts\Controller
 */
#[Route(path: '/layouts', name: 'kw.layouts.blog.', methods: ['GET'], format: 'html', utf8: true)]
final class BlogController extends AbstractController
{
    #[Route(path: '/{theme}/list.php', name: 'list')]
    public function list(string $theme): Response
    {
        return $this->render('@Layouts/themes/'.$theme.'/contents/blog/list.html.twig', [
            'controller_name' => 'Blog List',
        ]);
    }

    #[Route(path: '/{theme}/post.php', name: 'post')]
    public function post(string $theme): Response
    {
        return $this->render('@Layouts/themes/'.$theme.'/contents/blog/post.html.twig', [
            'controller_name' => 'Blog Post',
        ]);
    }
}
