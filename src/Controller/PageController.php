<?php

namespace KarlitoWeb\Layouts\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Attribute\Route;

/**
 * Class PageController
 *
 * @package KarlitoWeb\Layouts\Controller
 */
#[Route(path: '/layouts/{theme}/page', name: 'kw.layouts.page.', methods: ['GET'], format: 'html', utf8: true)]
final class PageController extends AbstractController
{
    #[Route(path: '/album.php', name: 'album')]
    public function album(string $theme): Response
    {
        return $this->render('@Layouts/themes/'.$theme.'/contents/page/album.html.twig', [
            'controller_name' => 'Album',
        ]);
    }

    #[Route(path: '/blank.php', name: 'blank')]
    public function blank(string $theme): Response
    {
        return $this->render('@Layouts/themes/'.$theme.'/contents/page/blank.html.twig', [
            'controller_name' => 'Blank Page',
        ]);
    }

    #[Route(path: '/coming-soon.php', name: 'comingsoon')]
    public function comingSoon(string $theme): Response
    {
        return $this->render('@Layouts/themes/'.$theme.'/contents/page/coming-soon.html.twig', [
            'controller_name' => 'Coming Soon',
        ]);
    }

    #[Route(path: '/cover.php', name: 'cover')]
    public function cover(string $theme): Response
    {
        return $this->render('@Layouts/themes/'.$theme.'/contents/page/cover.html.twig', [
            'controller_name' => 'Product',
        ]);
    }

    #[Route(path: '/off-line.php', name: 'offline')]
    public function offLine(string $theme): Response
    {
        return $this->render('@Layouts/themes/'.$theme.'/contents/page/off-line.html.twig', [
            'controller_name' => 'Off Line',
        ]);
    }

    #[Route(path: '/pricing.php', name: 'pricing')]
    public function pricing(string $theme): Response
    {
        return $this->render('@Layouts/themes/'.$theme.'/contents/page/pricing.html.twig', [
            'controller_name' => 'Pricing',
        ]);
    }

    #[Route(path: '/product.php', name: 'product')]
    public function product(string $theme): Response
    {
        return $this->render('@Layouts/themes/'.$theme.'/contents/page/product.html.twig', [
            'controller_name' => 'Product',
        ]);
    }
}
