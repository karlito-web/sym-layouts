<?php

namespace KarlitoWeb\Layouts\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Attribute\Route;

/**
 * Class FormController
 *
 * @package KarlitoWeb\Layouts\Controller
 */
#[Route(path: '/layouts/{theme}/form', name: 'kw.layouts.form.', methods: ['GET'], format: 'html', utf8: true)]
final class FormController extends AbstractController
{
	#[Route(path: '/layout.php', name: 'layout')]
    public function layout(string $theme): Response
    {
        return $this->render('@Layouts/themes/'.$theme.'/contents/form/layouts.html.twig', [
            'controller_name' => 'Welcome to Layouts',
        ]);
    }

    #[Route(path: '/element.php', name: 'element')]
    public function element(string $theme): Response
    {
        return $this->render('@Layouts/themes/'.$theme.'/contents/form/elements.html.twig', [
            'controller_name' => 'Welcome to Layouts',
        ]);
    }

    #[Route(path: '/validation.php', name: 'validation')]
    public function validation(string $theme): Response
    {
        return $this->render('@Layouts/themes/'.$theme.'/contents/form/validation.html.twig', [
            'controller_name' => 'Welcome to Layouts',
        ]);
    }

    #[Route(path: '/editor.php', name: 'editor')]
    public function editors(string $theme): Response
    {
        return $this->render('@Layouts/themes/'.$theme.'/contents/form/editors.html.twig', [
            'controller_name' => 'Welcome to Layouts',
        ]);
    }
}
