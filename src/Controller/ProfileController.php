<?php

namespace KarlitoWeb\Layouts\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Attribute\Route;

/**
 * Class ProfileController
 *
 * @package KarlitoWeb\Layouts\Controller
 */
#[Route(path: '/layouts/{theme}/profile', name: 'kw.layouts.profile.', methods: ['GET'], format: 'html', utf8: true)]
final class ProfileController extends AbstractController
{
    #[Route(path: '/my-profile.php', name: 'my-profile')]
    public function profile(string $theme): Response
    {
        return $this->render('@Layouts/themes/'.$theme.'/contents/profile/my-profile.html.twig', [
            'controller_name' => 'My Profile',
        ]);
    }

    #[Route(path: '/edit-profile.php', name: 'edit')]
    public function edit(string $theme): Response
    {
        return $this->render('@Layouts/themes/'.$theme.'/contents/profile/edit-my-profile.html.twig', [
            'controller_name' => 'Edit Profile',
        ]);
    }
}
