<?php

namespace KarlitoWeb\Layouts\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Attribute\Route;

/**
 * Class SecurityController
 *
 * @package KarlitoWeb\Layouts\Controller
 */
#[Route(path: '/layouts/{theme}', name: 'kw.layouts.security.', methods: ['GET'], format: 'html', utf8: true)]
final class SecurityController extends AbstractController
{
    #[Route(path: '/sign-in.php', name: 'signin', options: ['expose' => false])]
    public function signin(string $theme): Response
    {
        return $this->render('@Layouts/themes/'.$theme.'/contents/security/sign-in.html.twig', [
            'controller_name' => 'Sign In',
        ]);
    }

    #[Route(path: '/sign-in-1.php', name: 'signin-1', options: ['expose' => false])]
    public function signinv1(string $theme): Response
    {
        return $this->render('@Layouts/themes/'.$theme.'/contents/security/sign-in-1.html.twig', [
            'controller_name' => 'Sign In',
        ]);
    }

    #[Route(path: '/sign-in-2.php', name: 'signin-2', options: ['expose' => false])]
    public function signinv2(string $theme): Response
    {
        return $this->render('@Layouts/themes/'.$theme.'/contents/security/sign-in-2.html.twig', [
            'controller_name' => 'Sign In',
        ]);
    }

    #[Route(path: '/sign-up.php', name: 'signup', options: ['expose' => false])]
    public function signup(string $theme): Response
    {
        return $this->render('@Layouts/themes/'.$theme.'/contents/security/sign-up.html.twig', [
            'controller_name' => 'Sign Up',
        ]);
    }

    #[Route(path: '/forgot-password.php', name: 'forgot.password', options: ['expose' => false])]
    public function forgot_password(string $theme): Response
    {
        return $this->render('@Layouts/themes/'.$theme.'/contents/security/forgot-password.html.twig', [
            'controller_name' => 'Password Forgot',
        ]);
    }

    #[Route(path: '/lock-screen.php', name: 'lock.screen', options: ['expose' => false])]
    public function lock_screen(string $theme): Response
    {
        return $this->render('@Layouts/themes/'.$theme.'/contents/security/lock-screen.html.twig', [
            'controller_name' => 'Lock Screen',
        ]);
    }

    #[Route(path: '/confirm-mail.php', name: 'confirm.mail', options: ['expose' => false])]
    public function confirm_mail(string $theme): Response
    {
        return $this->render('@Layouts/themes/'.$theme.'/contents/security/confirm-mail.html.twig', [
            'controller_name' => 'Password Forgot',
        ]);
    }
}
