<?php

namespace KarlitoWeb\Layouts\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Attribute\Route;

/**
 * Class TableController
 *
 * @package KarlitoWeb\Layouts\Controller
 */
#[Route(path: '/layouts/{theme}/table', name: 'kw.layouts.table.', methods: ['GET'], format: 'html', utf8: true)]
final class TableController extends AbstractController
{
	#[Route(path: '/index.php', name: 'index')]
    public function index(string $theme): Response
    {
        return $this->render('@Layouts/themes/'.$theme.'/contents/table/index.html.twig', [
            'controller_name' => 'Welcome to Layouts',
        ]);
    }

	#[Route(path: '/data.php', name: 'data')]
    public function data(string $theme): Response
    {
        return $this->render('@Layouts/themes/'.$theme.'/contents/table/data.html.twig', [
            'controller_name' => 'Welcome to Layouts',
        ]);
    }
}
