<?php

namespace KarlitoWeb\Layouts\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Attribute\Route;

/**
 * Class ErrorController
 *
 * @package KarlitoWeb\Layouts\Controller
 */
#[Route(path: '/layouts/{theme}/error', name: 'kw.layouts.error.', methods: ['GET'], format: 'html', utf8: true)]
final class ErrorController extends AbstractController
{
    #[Route(path: '/403.php', name: 'error403')]
    public function error403(string $theme): Response
    {
        return $this->render('@Layouts/themes/'.$theme.'/contents/errors/403.html.twig', [
            'controller_name' => 'Error 403',
        ]);
    }

    #[Route(path: '/404.php', name: 'error404')]
    public function error404(string $theme): Response
    {
        return $this->render('@Layouts/themes/'.$theme.'/contents/errors/404.html.twig', [
            'controller_name' => 'Error 404',
        ]);
    }

    #[Route(path: '/500.php', name: 'error500')]
    public function error500(string $theme): Response
    {
        return $this->render('@Layouts/themes/'.$theme.'/contents/errors/500.html.twig', [
            'controller_name' => 'Error 500',
        ]);
    }
}
