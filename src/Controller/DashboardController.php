<?php

namespace KarlitoWeb\Layouts\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Attribute\Route;

/**
 * Class DashboardController
 *
 * @package KarlitoWeb\Layouts\Controller
 */
#[Route(path: '/layouts/{theme}/dashboard', name: 'kw.layouts.dashboard.', methods: ['GET'], format: 'html', utf8: true)]
final class DashboardController extends AbstractController
{
	#[Route(path: '/index.php', name: 'index')]
    public function index(string $theme): Response
    {
        return $this->render('@Layouts/themes/'.$theme.'/contents/dashboard/index.html.twig', [
            'controller_name' => 'Welcome to Layouts',
        ]);
    }
}
