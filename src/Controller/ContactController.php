<?php

namespace KarlitoWeb\Layouts\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Attribute\Route;

/**
 * Class ContactController
 *
 * @package KarlitoWeb\Layouts\Controller
 */
#[Route(path: '/layouts/{theme}/contact', name: 'kw.layouts.contact.', methods: ['GET'], format: 'html', utf8: true)]
final class ContactController extends AbstractController
{
	#[Route(path: '/index.php', name: 'index')]
    public function index(string $theme): Response
    {
        return $this->render('@Layouts/themes/'.$theme.'/contents/contact/index.html.twig', [
            'controller_name' => 'Welcome to Layouts',
        ]);
    }
}
