<?php

namespace KarlitoWeb\Layouts;

use KarlitoWeb\Layouts\DependencyInjection\LayoutsExtension;
use Symfony\Component\DependencyInjection\Extension\ExtensionInterface;
use Symfony\Component\HttpKernel\Bundle\Bundle;

/**
 * Class LayoutsBundle
 *
 * @package KarlitoWeb\Layouts\src\
 */
class LayoutsBundle extends Bundle
{
    /**
     * @return string
     */
    public function getNamespace(): string
    {
        return parent::getNamespace();
    }

    /**
     * @return string
     */
    public function getPath(): string
    {
        return \dirname(__DIR__);
    }

    /**
     * @return ExtensionInterface|null
     */
    public function getContainerExtension(): ?ExtensionInterface
    {
        return new LayoutsExtension();
    }
}
