<?php

namespace KarlitoWeb\Layouts\DependencyInjection;

use Exception;
use Symfony\Component\Config\FileLocator;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Extension\Extension;
use Symfony\Component\DependencyInjection\Loader\YamlFileLoader;

/**
 * Class LayoutsExtension
 *
 * @package KarlitoWeb\Layouts\DependencyInjection
 */
class LayoutsExtension extends Extension
{
    /**
     * @throws Exception
     */
    public function load(array $configs, ContainerBuilder $container): void
    {
        $loader = new YamlFileLoader(
            $container,
            new FileLocator(dirname(__DIR__, 2) . DIRECTORY_SEPARATOR . 'config')
        );

        // $loader->load($loader->getLocator()->locate('services.yaml'));
        $loader->load('services.yaml');
    }
}
