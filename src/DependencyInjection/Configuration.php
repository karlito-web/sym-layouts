<?php

namespace KarlitoWeb\Layouts\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

/**
 * Class Configuration
 *
 * @package KarlitoWeb\Layouts\DependencyInjection
 */
class Configuration implements ConfigurationInterface
{
	public function getConfigTreeBuilder(): TreeBuilder
	{
		return new TreeBuilder('Layouts');
	}
}
