## Add the namespace 

``` json
    "autoload": {
        "psr-4": {
            "App\\": "src/",
            "KarlitoWeb\\Layouts\\": "package/Layouts/src/"
        }
    },
    "autoload-dev": {
        "psr-4": {
            "App\\Tests\\": "tests/",
            "KarlitoWeb\\Layouts\\Tests\\": "package/Layouts/tests"
        }
    },
```
