``` cmd
------------------------------------- -------- -------- ------ ------------------------------------------- 
Name                                  Method   Scheme   Host   Path                                       
------------------------------------- -------- -------- ------ ------------------------------------------- 
kw.layouts.chart.chartjs              GET      ANY      ANY    /layouts/niceadmin/chart/chartjs.php         
kw.layouts.chart.apex                 GET      ANY      ANY    /layouts/niceadmin/chart/apex.php            
kw.layouts.chart.echart               GET      ANY      ANY    /layouts/niceadmin/chart/echart.php          
kw.layouts.contact.index              GET      ANY      ANY    /layouts/niceadmin/contact/index.php         
kw.layouts.dashboard.index            GET      ANY      ANY    /layouts/niceadmin/dashboard/index.php       
kw.layouts.error.error403             GET      ANY      ANY    /layouts/niceadmin/error/403.php             
kw.layouts.error.error404             GET      ANY      ANY    /layouts/niceadmin/error/404.php             
kw.layouts.error.error500             GET      ANY      ANY    /layouts/niceadmin/error/500.php             
kw.layouts.form.layout                GET      ANY      ANY    /layouts/niceadmin/form/layout.php           
kw.layouts.form.element               GET      ANY      ANY    /layouts/niceadmin/form/element.php          
kw.layouts.form.validation            GET      ANY      ANY    /layouts/niceadmin/form/validation.php       
kw.layouts.form.editors               GET      ANY      ANY    /layouts/niceadmin/form/editors.php          
kw.layouts.page.blank                 GET      ANY      ANY    /layouts/niceadmin/page/blank.php            
kw.layouts.profile.my-profile         GET      ANY      ANY    /layouts/niceadmin/profile/my-profile.php    
kw.layouts.security.signin            GET      ANY      ANY    /layouts/niceadmin/sign-in.php               
kw.layouts.security.signup            GET      ANY      ANY    /layouts/niceadmin/sign-up.php               
kw.layouts.table.index                GET      ANY      ANY    /layouts/niceadmin/table/index.php           
kw.layouts.table.data                 GET      ANY      ANY    /layouts/niceadmin/table/data.php            
------------------------------------- -------- -------- ------ ------------------------------------------- 
```