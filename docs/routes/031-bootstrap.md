``` cmd
------------------------------------- -------- -------- ------ -------------------------------------------
Name                                  Method   Scheme   Host   Path
------------------------------------- -------- -------- ------ -------------------------------------------
kw.layouts.blog.list                  GET      ANY      ANY    /layouts/bootstrap/list.php
kw.layouts.dashboard.index            GET      ANY      ANY    /layouts/bootstrap/dashboard/index.php
kw.layouts.page.album                 GET      ANY      ANY    /layouts/bootstrap/page/album.php
kw.layouts.page.blank                 GET      ANY      ANY    /layouts/bootstrap/page/blank.php
kw.layouts.page.cover                 GET      ANY      ANY    /layouts/bootstrap/page/cover.php
kw.layouts.page.pricing               GET      ANY      ANY    /layouts/bootstrap/page/pricing.php
kw.layouts.page.product               GET      ANY      ANY    /layouts/bootstrap/page/product.php
kw.layouts.security.signin            GET      ANY      ANY    /layouts/bootstrap/sign-in.php
kw.layouts.security.signup            GET      ANY      ANY    /layouts/bootstrap/sign-up.php
------------------------------------ -------- -------- ------ -------------------------------------------
```