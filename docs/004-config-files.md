# Routes
## Create a files in ``config/routes`` directory
ex: ``config/routes/bundle_layouts.yaml``

## Add
``` yaml
layouts:
    resource: '@LayoutsBundle/config/routes.yaml'
```

# Edit files
- ``config/routes/framework.yaml``

``` yaml
    # assets configuration
    assets:
        # Throw an exception if an entry is missing from the manifest.json
        enabled:              true
        strict_mode:          true
        packages:
            # Prototype
            favicons:
                # Throw an exception if an entry is missing from the manifest.json
                strict_mode:          true
                version:              '1.0'
                version_format:       '%%s?v=%%s'
                base_path:            '/bundles/layouts/favicons/'
            images:
                # Throw an exception if an entry is missing from the manifest.json
                strict_mode:          true
                version:              '1.0'
                version_format:       '%%s?v=%%s'
                base_path:            '/bundles/layouts/images/'
            themes:
                # Throw an exception if an entry is missing from the manifest.json
                strict_mode:          true
                version:              '1.0'
                version_format:       '%%s?v=%%s'
                base_path:            '/bundles/layouts/themes/'

    # Uid configuration
    uid:
        enabled:                    true
        default_uuid_version:       7 # One of 7; 6; 4; 1
        name_based_uuid_version:    5 # One of 5; 3
        time_based_uuid_version:    7 # One of 7; 6; 1
```

- ``config/routes/twig.yaml``

``` yaml
    paths:
        '%kernel.project_dir%/templates':                 'App'
        '%kernel.project_dir%/package/Layouts/templates': 'Layouts'
```
