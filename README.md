# Layouts
###### by Karlito-Web

### Installation & Usages
- [Doc](docs/001-doc-official.md)

### Symfony Configurations
- [Composer.jspn](docs/002-composer.json.md)
- [Bundles.php](docs/003-bundles.php.md)
- [Config](docs/004-config-files.md)

### Templates
- [Structure](docs/structures/001-base.md)
- [Template NiceAdmin](docs/structures/051-niceadmin.md)

---